<?php

use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userData = [];
        $faker = Faker::create();

        for ($userCount = 0; $userCount < 50; $userCount++) {
            $userData[$userCount]['name'] = $faker->name;
            $userData[$userCount]['email'] = $faker->email;
            $userData[$userCount]['email_verified_at'] = $faker->dateTimeBetween('-11 month', '-1 day');
            $userData[$userCount]['password'] = bcrypt('281749799s');
        }
        User::query()->insert($userData);
    }
}
