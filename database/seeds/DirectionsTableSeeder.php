<?php

use App\Models\Direction;
use Illuminate\Database\Seeder;

class DirectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $directions = Direction::getDirections();
        Direction::query()->insert($directions);
    }
}
