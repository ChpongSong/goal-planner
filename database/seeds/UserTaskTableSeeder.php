<?php

use App\Models\UserTask;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserTaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tasks = [];
        $faker = Faker::create();
        $usersId = User::query()
            ->pluck('id')
            ->toArray();

        for ($task = 1; $task < 50; $task++) {
            $tasks[$task]['user_id'] = array_rand($usersId);
            $tasks[$task]['direction_id'] = rand(1, 4);
            $tasks[$task]['objective'] = $faker->text;
            $tasks[$task]['description'] = $faker->realText(rand(20, 50));
            $tasks[$task]['status'] = \App\Models\UserTask::TASK_STATUS_IN_PROGRESS;
        }

        UserTask::query()->insert($tasks);
    }
}
