<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/tasks/show');
});

Auth::routes();

Route::group(['prefix' => 'tasks', 'middleware' => ['auth']], function () {
    Route::get('/show', 'UserTaskController@index')->name('home');
    Route::get('/create', 'UserTaskController@createTask');
    Route::post('/', 'UserTaskController@store');
    Route::get('/{task}/edit', 'UserTaskController@editTask');
    Route::patch('/{task}', 'UserTaskController@updateTask');
    Route::delete('/{task}', 'UserTaskController@deleteTask');
    Route::get('/make/direction', 'UserTaskController@makeDirection');
    Route::post('/make/direction', 'UserTaskController@storeDirection');
});

