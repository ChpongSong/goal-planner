<?php

namespace App\Models;

use App\Models\UserTask;
use Illuminate\Database\Eloquent\Model;

class Direction extends Model
{

    protected $fillable = ['name'];
    /**
     * @return array
     */
    public static function getDirections()
    {
        return [
            'саморазвитие',
            'учеба',
            'работа',
            'бытовуха'
        ];
    }


    public function task()
    {
        return  $this->hasMany(UserTask::class);
    }
}
