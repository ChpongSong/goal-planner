<?php

namespace App\Models;

use App\User;
use App\Models\Direction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTask extends Model
{
    use SoftDeletes;

    const TASK_STATUS_DONE = "done";
    const TASK_STATUS_IN_PROGRESS = "in progress";

    protected $fillable = [
        'user_id',
        'direction_id',
        'objective',
        'description',
        'status',
        'priority'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function direction()
    {
        return $this->belongsTo(Direction::class);
    }
}
