<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDirectionRequest;
use App\Http\Requests\UserTaskStoreRequest;
use App\Http\Requests\UserTaskUpdateRequest;
use App\Models\Direction;
use App\Models\UserTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserTaskController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $tasks = UserTask::query()->where('user_id', $user->id)->get();
        return view('tasks.index', compact('tasks'));
    }

    public function store(UserTaskStoreRequest $request)
    {
        $user = Auth::user();
        UserTask::create([
            'user_id' => $user->id,
            'direction_id' => rand(1, 4),
            'objective' => $request->get('objective'),
            'description' => $request->get('description'),
            'priority' => $request->get('priority'),
            'status' => UserTask::TASK_STATUS_IN_PROGRESS
        ]);
        return redirect('/tasks/show');
    }

    public function createTask()
    {
        $user = Auth::user();
        $directions = Direction::query()->get(['id', 'name']);
        return view('tasks.create', compact('directions'));
    }

    public function editTask($id)
    {
        $user = Auth::user();
        $tasks = UserTask::query()->where(['user_id' => $user->id, 'id' => $id])->first();
        $directions = Direction::query()->get(['id', 'name']);
        return view('tasks.edit', compact('directions', 'tasks'));
    }

    public function updateTask(UserTaskUpdateRequest $request, $id)
    {
        UserTask::query()->where('id', $id)->update([
            'objective' => $request->get('objective'),
            'description' => $request->get('description'),
            'priority' => $request->get('priority'),
            'direction_id' => $request->get('direction_id'),
        ]);
        return redirect('/tasks/show');
    }

    public function deleteTask($id)
    {
        $user = Auth::user();
        $tasks = UserTask::query()->where('id', $id)->first();
        if ($tasks->status === UserTask::TASK_STATUS_IN_PROGRESS) {
            $tasks->update([
                'status' => UserTask::TASK_STATUS_DONE
            ]);

            return redirect('tasks/show');
        }
        $tasks->update([
            'status' => UserTask::TASK_STATUS_IN_PROGRESS
        ]);

        return redirect('tasks/show');
    }

    public function makeDirection()
    {
        return view('tasks.make.direction');
    }

    public function storeDirection(StoreDirectionRequest $request)
    {
        Direction::create([
           'name' => $request->get('name')
        ]);
        return redirect('/tasks/show');
    }
}
