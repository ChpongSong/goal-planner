@extends('layouts.app')

@section('content')
   <div class="container">
       <div class="row">
           <div class="col">
               <a href="/tasks/create" class="btn btn-info">
                   Create Objective
               </a>
           </div>
           <div class="row">
               <div class="col">
                   <a href="/tasks/make/direction" class="btn btn-info">
                       Make Direction
                   </a>
               </div>
           </div>
       </div>

       <table class="table">
           @foreach($tasks as $task)
               <tr>
                   <td>
                       <button class="btn-success">
                           {{ $task->status }}
                       </button>
                   </td>
                   <td>
                       <button class="btn-warning">
                           Priority:{{ $task->priority }}
                       </button>
                   </td>
               </tr>
               <tr>
                   <td>
                       <a href="/tasks/{{ $task->id }}/edit">
                           {{ $task->objective }}
                       </a>
                   </td>
               </tr>

               <tr>
                   <td>
                       Owner:{{ $task->user->name }}
                   </td>
               </tr>
               <tr>
                   <td>
                       Direction:{{ $task->direction->name }}
                   </td>
               </tr>

               <tr>
                   <td>
                       Description:{{ $task->description }}
                   </td>
               </tr>
               <tr>
                   <td>
                       <form action="/tasks/{{ $task->id }}" method="POST">
                           @method('DELETE')
                           @csrf
                           <button type="submit" class="btn btn-danger">Change Status</button>
                       </form>
                   </td>
               </tr>

           @endforeach
       </table>
   </div>
@endsection
