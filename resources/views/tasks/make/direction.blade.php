@extends('layouts.app')


@section('content')
    <div class="container">
        <form action="/tasks/make/direction" method="POST">
            @csrf

            <div class="form-group">
                <label for="direction">Direction:</label>
                <input type="text" class="form-control" placeholder="Enter direction" id="direction" name="name">
            </div>

            <button type="submit" class="btn-success">Create Direction</button>
        </form>
    </div>
@endsection
