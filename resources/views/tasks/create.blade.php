@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="/tasks" method="POST">
            @csrf
            <div class="form-group">
                <label for="objective">Objective:</label>
                <input type="text" class="form-control" placeholder="Напишите вашу цель" id="objective"
                       name="objective">
            </div>
            <div class="form-group">
                <label for="desc">Description:</label>
                <input type="text" class="form-control" placeholder="Описание" id="desc" name="description">
            </div>
            <div class="form-group">
                <label for="priority">Priority:</label>
                <input type="number" class="form-control" placeholder="Выберите приоритет задачи" id="priority" name="priority" max="5">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Directions</label>
                <select class="form-control" id="exampleFormControlSelect1" name="direction_id">
                    @foreach($directions as $direction)
                        <option value="{{ $direction->id }}">{{ $direction->name }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn-success">Create Objective</button>
        </form>
    </div>
@endsection
